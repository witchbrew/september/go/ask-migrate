package main

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/witchbrew/go/migratecliutils"
	"gitlab.com/witchbrew/go/migratecliutils/migrateclipostgres"
	"gitlab.com/witchbrew/september/go/ask-migrate/migrations"
	"os"
)

func main() {
	cmd := &cobra.Command{
		Use: "ask-migrate",
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			drv, err := migrateclipostgres.Driver("ask_version")
			if err != nil {
				return errors.Wrap(err, "failed to create postgres driver")
			}
			migratecliutils.SetupMigrate(drv, migrations.Migrations)
			migratecliutils.SetupLogger()
			return nil
		},
	}
	migrateclipostgres.AddPersistentFlags(cmd)
	migratecliutils.AddAllCommands(cmd)
	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
