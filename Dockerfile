FROM golang:1.15.2-alpine3.12

RUN apk update
RUN apk add git

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY . ./
RUN go build

FROM alpine:3.12
COPY --from=0 /app/ask-migrate .
ENTRYPOINT ["./ask-migrate"]