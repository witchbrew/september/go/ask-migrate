package migrations

import (
	"context"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/postgres"
)

var questionsTableUpQuery = `
CREATE TABLE questions
(
    id BIGSERIAL PRIMARY KEY,
    text TEXT NOT NULL
)
`
var questionsTableDownQuery = "DROP TABLE questions"

var optionsTableUpQuery = `
CREATE TABLE options
(
    question_id BIGINT NOT NULL ,
    index INTEGER NOT NULL,
    text TEXT NOT NULL,
    PRIMARY KEY(question_id, index)
)
`
var optionsTableDownQuery = "DROP TABLE options"

var Migrations = migrate.Migrations{
	{
		Up: func(ctx context.Context) error {
			db := postgres.ContextDB(ctx)
			_, err := db.Exec(questionsTableUpQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(optionsTableUpQuery)
			if err != nil {
				return err
			}
			return nil
		},
		Down: func(ctx context.Context) error {
			db := postgres.ContextDB(ctx)
			_, err := db.Exec(optionsTableDownQuery)
			if err != nil {
				return err
			}
			_, err = db.Exec(questionsTableDownQuery)
			if err != nil {
				return err
			}
			return nil
		},
	},
}
