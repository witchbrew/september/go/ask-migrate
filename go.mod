module gitlab.com/witchbrew/september/go/ask-migrate

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	gitlab.com/witchbrew/go/migrate v0.0.0-20201001153901-86e0e45cf03e
	gitlab.com/witchbrew/go/migratecliutils v0.0.0-20201002080224-57870c7254e4
)
